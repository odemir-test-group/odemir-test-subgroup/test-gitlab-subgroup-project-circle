def _check_code_style(self, code):
    style_guide = pycodestyle.StyleGuide()
    result = style_guide.check_code(code)
    if result.total_errors:
        self.feedback.append(
            "Code style issues found. Please check and fix them.")