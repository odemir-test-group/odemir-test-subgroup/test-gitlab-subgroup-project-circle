if __name__ == "__main__":
    # Example Python code to analyze
    python_code = """
    def add(a, b):
        result = a + b
        print(result)
    """

    code_reviewer = CodeReviewer()
    code_reviewer.analyze_python_code(python_code)

    feedback = code_reviewer.get_feedback()

    if feedback:
        print("Code Review Feedback:")
        for msg in feedback:
            print(f"- {msg}")
    else:
        print("No coding errors found. Code looks good!")